Rails.application.routes.draw do


  # mount Blazer::Engine, at: "blazer"
  namespace :admin do
    devise_scope :user do
      get "/sign_in" => "devise/sessions#new" # custom path to login/sign_in
      get "/sign_up" => "devise/registrations#new", as: "new_user_registration" # custom path to sign_up/registration
    end

    devise_for :users, controllers: { sessions: 'admin/users/sessions'}, :skip => [:registrations]
      as :user do
        get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
        put 'users' => 'devise/registrations#update', :as => 'user_registration'
        patch 'users' => 'devise/registrations#update', :as => 'update_user_registration'
    end

    root "dashboard#index"
    get "dashboard" => "dashboard#index", :as => :dashboard
    resources :users
    resources :settings, only: [:new, :create, :edit, :update]
    resources :personalities
    resources :services
    resources :experiences
    resources :educations
    resources :skills
    resources :blogs
    resources :categories
    resources :achivements
    resources :visits
    resources :contacts, only: [:index, :show]
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "homes#index"
  resources :categories do
    resources :blogs
  end

  resources :blogs
  resources :homes
  resources :services
  resources :achivements

  #Api
  namespace :api do
    namespace :v1 do
      resources :blogs, only: [:index, :show]
    end
  end

  namespace :demo do
    namespace :v1 do
      get 'signup' => 'demo_users#new', :as => :signup
      get 'logout' => 'sessions#destroy', :as => :logout
      get 'login' => 'sessions#new', :as => :login
      resources :sessions
      resources :demo_users

      resources :todos
      root "todos#index"
    end
  end
end
