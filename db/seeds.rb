# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
user = CreateAdminService.new.call
puts 'CREATED ADMIN USER: ' << user.email

5.times do
  personality = Personality.create(
    full_name: Faker::Name.name,
    email: Faker::Internet.email,
    phone: Faker::PhoneNumber.cell_phone,
    dateofbirth: Faker::Date.birthday(18, 65),
    address: Faker::Address.street_address,
    nationality: Faker::Lorem.word,
    description: Faker::Lorem.sentence,
    user: user
  )

  service = Service.create(
    title: Faker::Lorem.word,
    description: Faker::Lorem.sentence,
    icon: Faker::Lorem.word,
    user: user
  )

  experience = Experience.create(
    title: Faker::Lorem.word,
    description: Faker::Lorem.sentence,
    icon: Faker::Lorem.word,
    achive_date: Faker::Date.between(2.days.ago, Date.today),
    user: user
  )

  education = Education.create(
    title: Faker::Lorem.word,
    icon: Faker::Lorem.word,
    description: Faker::Lorem.sentence,
    start_date: Faker::Date.between(5.days.ago, Date.today),
    finished_date: Faker::Date.between(2.days.ago, Date.today),
    school_name: Faker::Address.community,
    user: user
  )

  category = Category.create(
    category_name: Faker::Lorem.word,
    user: user
  )


  achivement = Achivement.create(
    title: Faker::Lorem.word,
    finishe_on: Faker::Date.between(2.days.ago, Date.today),
    category: category,
    client: Faker::ProgrammingLanguage.creator,
    live_demo: Faker::Lorem.word,
    description: Faker::Lorem.sentence,
    user: user
  )

  skill = Skill.create(
    skill_name: Faker::ProgrammingLanguage.name,
    percentage: Faker::Number.between(1, 100),
    achivement: achivement,
    user: user
  )

  blog = Blog.create(
    title: Faker::Lorem.word,
    description: Faker::Lorem.sentence,
    author: Faker::ProgrammingLanguage.creator,
    post_date: Time.now,
    status: [true,false].sample,
    category: category,
    user: user
  )

end
