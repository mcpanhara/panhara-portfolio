class AddDemoUserIdToTodos < ActiveRecord::Migration[5.1]
  def change
    add_column :todos, :demo_user_id, :integer
  end
end
