class AddFieldsToSettings < ActiveRecord::Migration[5.1]
  def change
    add_column :settings, :footer_title, :string
    add_column :settings, :location, :string
    add_column :settings, :email, :string
    add_column :settings, :url, :string
  end
end
