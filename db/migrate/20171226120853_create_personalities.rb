class CreatePersonalities < ActiveRecord::Migration[5.1]
  def change
    create_table :personalities do |t|
      t.string :full_name
      t.string :email
      t.string :phone
      t.date :dateofbirth
      t.string :address
      t.string :nationality
      t.text :description
      t.string :image

      t.timestamps
    end
  end
end
