class AddUserIdToPersonality < ActiveRecord::Migration[5.1]
  def change
    add_column :personalities, :user_id, :integer
  end
end
