class CreateExperiences < ActiveRecord::Migration[5.1]
  def change
    create_table :experiences do |t|
      t.string :title
      t.text :description
      t.string :icon
      t.date :achive_date

      t.timestamps
    end
  end
end
