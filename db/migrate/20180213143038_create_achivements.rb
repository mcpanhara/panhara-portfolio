class CreateAchivements < ActiveRecord::Migration[5.1]
  def change
    create_table :achivements do |t|
      t.string :title
      t.date :finishe_on
      t.string :image
      t.integer :category_id
      t.integer :user_id
      t.string :client
      t.string :live_demo
      t.text :description
      t.string :slug

      t.timestamps
    end
  end
end
