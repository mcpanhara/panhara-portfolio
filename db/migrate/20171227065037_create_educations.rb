class CreateEducations < ActiveRecord::Migration[5.1]
  def change
    create_table :educations do |t|
      t.string :title
      t.string :icon
      t.text :description
      t.date :start_date
      t.date :finished_date
      t.string :school_name

      t.timestamps
    end
  end
end
