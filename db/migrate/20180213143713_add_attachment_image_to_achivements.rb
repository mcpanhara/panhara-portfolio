class AddAttachmentImageToAchivements < ActiveRecord::Migration[5.1]
  def self.up
    change_table :achivements do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :achivements, :image
  end
end
