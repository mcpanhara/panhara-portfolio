class Category < ApplicationRecord
  has_many :blogs
  belongs_to :user
  has_many :achivements
end
