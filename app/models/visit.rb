class Visit < ActiveRecord::Base
  has_many :ahoy_events, class_name: "Ahoy::Event"
  belongs_to :home, optional: true
  belongs_to :blog, optional: true
end
