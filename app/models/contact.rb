class Contact < ApplicationRecord
  validates :full_name, presence: true
  validates_format_of :email, :with => /\A[-a-z0-9_+\.]+\@([-a-z0-9]+\.)+[a-z0-9]{2,4}\z/i
  validates :email, presence: true
  validates :phone, numericality: true
end
