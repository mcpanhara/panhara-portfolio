class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  enum role: [:user, :vip, :admin, :staff]
  after_initialize :set_default_role, :if => :new_record?
  after_initialize :set_role_vip, :if => :new_record?

  has_many :personalities
  has_many :services
  has_many :experiences
  has_many :educations
  has_many :skills
  has_many :blogs
  has_many :categories
  has_many :achivements

  #instance method
  def set_default_role
    self.role ||= :user
  end

  def set_role_vip
    self.role ||= :vip
  end


  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  # validates :username, presence: true, uniqueness: true
  # validates :address, presence: true
  # validates :phone, presence: true, numericality: true
  # validates :image, presence: true
end
