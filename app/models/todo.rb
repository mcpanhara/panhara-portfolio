class Todo < ApplicationRecord
  belongs_to :demo_user

  validates :title, presence: true
end
