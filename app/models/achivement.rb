class Achivement < ApplicationRecord
  before_save :update_slug

  def update_slug
    self.slug = title.parameterize
  end

  def to_param
    slug
  end

  belongs_to :user
  has_many :skills, inverse_of: :achivement
  belongs_to :category
  accepts_nested_attributes_for :skills, reject_if: :all_blank, allow_destroy: true
  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
