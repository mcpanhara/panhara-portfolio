class BlogsController < FrontEndApplicationController
  before_action :set_setting
  def index
    @blogs = Blog.all.where({status: true}).order(id: :desc).page params[:page]
    @categoryblogs = Category.all.limit(5)
  end

  def show
    @blog = Blog.find_by_slug(params[:id])
    @category = Category.all.limit(10)
    @blogs = Blog.all.limit(10)
  end

  private
    def set_setting
      @settings = Setting.all.limit(1)
    end
end
