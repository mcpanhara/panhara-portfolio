class Admin::BlogsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_blog, only: [:show, :edit, :update, :destroy]
  before_action :set_administrator
  before_action :admin_only, only:[:destroy]
  def index
    @search = Blog.ransack(params[:search])
    @blogs = @search.result.order(id: :desc)
    @admin = User.where({role: "admin"})
    @approved = Blog.where({status: "Approved"})
  end

  def show
  end

  def new
    @blog = current_admin_user.blogs.build
  end

  def create
    @blog = current_admin_user.blogs.build(blog_params)
    if @blog.save
      redirect_to admin_blog_path(@blog), notice: "Successfully Created"
    else
      render :new
    end
  end

  def edit

  end

  def update
    if @blog.update(blog_params)
      redirect_to admin_blog_path(@blog), notice: "Successfully Updated"
    else
      render :edit
    end
  end

  def destroy
    if @blog.destroy
      redirect_to admin_blogs_path
    end
  end

  private
    def set_blog
      @blog = Blog.find_by_slug(params[:id])
    end

    def blog_params
      params.require(:blog).permit(:title,:description,:image,:author,:category_id,:post_date, :user_id, :slug,:status)
    end

    def admin_only
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, :notice => "Access denied."
        end
      end
    end

    def set_administrator
      @admin = User.where({role: "admin"})
    end
end
