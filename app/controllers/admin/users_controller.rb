class Admin::UsersController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_user, only: [:show,:edit,:update,:destroy]
  before_action :set_administrator
  before_action :admin_only, only: [:edit,:update,:destroy,:create]

  def index
    @search = User.ransack(params[:search])
    @users = @search.result
  end

  def show
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(secure_params)
    if @user.save
      redirect_to admin_user_path(@user), notice: "User Was Created Successfully"
    else
      flash[:error] = "Something Went Wrong See The Message Below"
      render :new
    end
  end

  def edit
  end

  def update
    no_requried_password
    if @user.update_attributes(user_params)
      redirect_to admin_user_path(@user), notice: "Updated Was Successfully"
    else
      flash[:error] = "Something Went Wrong See The Message Below"
      render :edit
    end
  end

  def destroy
    if @user.destroy
      redirect_to admin_users_path, notice: "User Deleted Successfully"
    end
  end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def no_requried_password
      if params[:user][:password].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
    end

    def admin_only
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, :notice => "Access denied."
        end
      end
    end

  def secure_params
    params.require(:user).permit(:username,:email,:password,:password_confirmation,
                                  :role,:description,:image,:position,:dateofbirth,
                                  :address,:phone)
  end
  def user_params
    params.require(:user).permit(:username,:email,:password,
                                  :role,:description,:image,:position,:dateofbirth,
                                  :address,:phone)
  end

  def set_administrator
    @admin = User.where({role: "admin"})
  end
end
