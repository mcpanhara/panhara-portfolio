class Admin::VisitsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_activity, only: [:show, :edit, :update, :destroy]
  before_action :set_administrator
  before_action :admin_only, only:[:destroy]

  def index
    @search = Visit.ransack(params[:search])
    @visitors = @search.result.order(id: :desc)
  end

  def show

  end

  private
    def admin_only
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, :notice => "Access denied."
        end
      end
    end

    def set_administrator
      @admin = User.where({role: "admin"})
    end

    def set_activity
      @visitor = Visit.find(params[:id])
    end
end
