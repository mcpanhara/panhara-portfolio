class Admin::PersonalitiesController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_personality, only: [:show, :edit, :update, :destroy]
  before_action :set_administrator
  before_action :admin_only, except: [:index,:show]
  def index
    @search = Personality.ransack(params[:search])
    @personalities = @search.result
  end

  def new
    @personality = current_admin_user.personalities.build
  end

  def create
    @personality = current_admin_user.personalities.build(personality_params)
    if @personality.save
      redirect_to admin_personality_path(@personality), notice: "Successfully Created"
    else
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @personality.update(personality_params)
      redirect_to admin_personality_path(@personality), notice: "Successfully Updated"
    else
      render :edit
    end
  end

  def destroy
    if @personality.destroy
      redirect_to admin_personalities_path
    end
  end

  private
    def set_personality
      @personality = Personality.find(params[:id])
    end

    def personality_params
      params.require(:personality).permit(:full_name, :email, :phone, :nationality, :user_id, :dateofbirth, :description, :address, :image)
    end

    def admin_only
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, :notice => "Access denied."
        end
      end
    end

    def set_administrator
      @admin = User.where({role: "admin"})
    end
end
