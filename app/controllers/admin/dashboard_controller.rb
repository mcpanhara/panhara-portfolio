class Admin::DashboardController < ApplicationController
  before_action :authenticate_admin_user!, only: [:index]
  def index
    @admin = User.where({role: "admin"})
    @visitors = Visit.all
    @ahoy_events = Ahoy::Event.all.count
  end
end
