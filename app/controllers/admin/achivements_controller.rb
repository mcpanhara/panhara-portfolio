class Admin::AchivementsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_achivement, only: [:show, :edit, :update, :destroy]
  before_action :set_administrator
  before_action :admin_only, except: [:index,:show]
  def index
    @search = Achivement.ransack(params[:search])
    @achivements = @search.result
  end

  def show
  end

  def new
    @achivement = current_admin_user.achivements.build
  end

  def create
    @achivement = current_admin_user.achivements.build(achivement_params)
    if @achivement.save
      redirect_to admin_achivement_path(@achivement), notice: "Successfully Created"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @achivement.update(achivement_params)
      redirect_to admin_achivement_path(@achivement), notice: "Successfully Updated"
    else
      render :edit
    end
  end

  def destroy
    if @achivement.destroy
      redirect_to admin_achivements_path, notice: "Successfully Deleted"
    end
  end

  private
    def set_achivement
      @achivement = Achivement.find_by_slug(params[:id])
    end

    def admin_only
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, :notice => "Access denied."
        end
      end
    end

    def achivement_params
      params.require(:achivement).permit(:title,:finishe_on, :user_id, :category_id, :client, :live_demo, :image, skill_ids: [])
    end
    def set_administrator
      @admin = User.where({role: "admin"})
    end
end
