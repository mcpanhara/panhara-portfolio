class Admin::CategoriesController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  before_action :set_administrator
  def index
    @search = Category.ransack(params[:search])
    @categories = @search.result
  end

  def show

  end

  def new
    @category = current_admin_user.categories.build
  end

  def create
    @category = current_admin_user.categories.build(category_params)
    if @category.save
      redirect_to admin_category_path(@category), notice: "Successfully Created"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @category.update(category_params)
      redirect_to admin_category_path(@category), notice: "Successfully Created"
    else
      render :edit
    end
  end

  def destroy
    if @category.destroy
      redirect_to admin_categories_path
    end
  end

  private
    def set_category
      @category = Category.find(params[:id])
    end

    def category_params
      params.require(:category).permit(:category_name, :user_id)
    end

    def set_administrator
      @admin = User.where({role: "admin"})
    end
end
