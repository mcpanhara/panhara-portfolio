class Admin::ContactsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :admin_only?

  def index
    @search = Contact.ransack(params[:search])
    @contacts = @search.result.order(id: :desc)
  end

  def show
    @contact = Contact.find(params[:id])
  end

  private
    def admin_only?
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, notice: "Access Denied"
        end
      end
    end
end
