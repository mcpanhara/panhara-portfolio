class Admin::ServicesController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_service, only: [:show, :edit, :update, :destroy]
  before_action :set_administrator
  before_action :admin_only, except: [:index,:show]
  def index
    @search = Service.ransack(params[:search])
    @services = @search.result
  end

  def show
  end

  def new
    @service = current_admin_user.services.build
  end

  def create
    @service = current_admin_user.services.build(service_params)
    if @service.save
      redirect_to admin_service_path(@service), notice: "Successfully Created"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @service.update(service_params)
      redirect_to admin_service_path(@service), notice: "Successfully Updated"
    else
      render :edit
    end
  end

  def destroy
    if @service.destroy
      redirect_to admin_services_path, notice: "Successfully Deleted"
    end
  end

  private
    def set_service
      @service = Service.find(params[:id])
    end

    def service_params
      params.require(:service).permit(:title, :description, :icon, :user_id)
    end

    def admin_only
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, :notice => "Access denied."
        end
      end
    end

    def set_administrator
      @admin = User.where({role: "admin"})
    end
end
