class Admin::SettingsController < ApplicationController
  before_action :authenticate_admin_user!, only: [:new, :create, :update, :edit, :destroy]
  before_action :set_administrator
  before_action :admin_only, except: [:index,:show]
  def new
    if Setting.any?
      redirect_to edit_admin_setting_path(Setting.first)
    else
      @setting = Setting.new
      @social = @setting.socials.build
    end
  end

  def create
    @setting = Setting.new(setting_params)
    if @setting.save
      redirect_to edit_admin_setting_path(@setting), notice: "Successfully Created"
    else
      flash[:alert] = 'There was a problem creating setting'
      render :new
    end
  end

  def edit
    @setting = Setting.find(params[:id])
    if !@setting.socials.exists?
      1.times do
        @social = @setting.socials.build
      end
    else
      @setting = Setting.find(params[:id])
    end
  end

  def update
    @setting = Setting.find(params[:id])
    if @setting.update(setting_params)
      redirect_to edit_admin_setting_path(@setting), notice: "Update Was Successfully"
    else
      flash[:alert] = "There waws a problem updating setting"
      render :edit
    end
  end

  private
    def setting_params
      params.require(:setting).permit(:site_name, :post_per_page,
                                      :under_maintenace, :prevent_commenting,
                                      :tag_visibility,:footer_title,:location,:email,:url, socials_attributes: [:id, :destroy, :social_media])
    end
    def set_administrator
      @admin = User.where({role: "admin"})
    end
    def admin_only
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, :notice => "Access denied."
        end
      end
    end
end
