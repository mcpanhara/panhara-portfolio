class Admin::ExperiencesController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_experience, only: [:show, :edit, :update, :destroy]
  before_action :set_administrator
  before_action :admin_only, except: [:index,:show]
  def index
    @search = Experience.ransack(params[:search])
    @experiences = @search.result
  end

  def show
  end

  def new
    @experience = current_admin_user.experiences.build
  end

  def create
    @experience = current_admin_user.experiences.build(experience_params)
    if @experience.save
      redirect_to admin_experience_path(@experience), notice: "Successfully Created"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @experience.update(experience_params)
      redirect_to admin_experience_path(@experience), notice: "Successfully Updated"
    else
      render :edit
    end
  end

  def destroy
    if @experience.destroy
      redirect_to admin_experiences_path, notice: "Successfully Deleted"
    end
  end

  private
    def set_experience
      @experience = Experience.find(params[:id])
    end

    def experience_params
      params.require(:experience).permit(:title,:description, :achive_date,:icon, :user_id)
    end

    def admin_only
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, :notice => "Access denied."
        end
      end
    end

    def set_administrator
      @admin = User.where({role: "admin"})
    end
end
