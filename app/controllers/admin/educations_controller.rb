class Admin::EducationsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_education, only: [:show, :edit, :update, :destroy]
  before_action :set_administrator
  before_action :admin_only, except: [:index,:show]
  def index
    @search = Education.ransack(params[:search])
    @educations = @search.result
  end

  def show
  end

  def new
    @education = current_admin_user.educations.build
  end

  def create
    @education = current_admin_user.educations.build(education_params)
    if @education.save
      redirect_to admin_education_path(@education), notice: "Successfully Created"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @education.update(education_params)
      redirect_to admin_education_path(@education), notice: "Successfully Updated"
    else
      render :edit
    end
  end

  def destroy
    if @education.destroy
      redirect_to admin_educations_path, notice: "Successfully Deleted"
    end
  end

  private
    def set_education
      @education = Education.find(params[:id])
    end

    def education_params
      params.require(:education).permit(:title,:description,:school_name,:start_date,:finished_date,:icon,:user_id)
    end

    def admin_only
      unless current_admin_user.admin?
        unless @user == current_admin_user
          redirect_to admin_dashboard_path, :notice => "Access denied."
        end
      end
    end

    def set_administrator
      @admin = User.where({role: "admin"})
    end
end
