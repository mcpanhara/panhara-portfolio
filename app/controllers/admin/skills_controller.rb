class Admin::SkillsController < ApplicationController
  before_action :authenticate_admin_user!
  before_action :set_skill, only: [:show, :edit, :update, :destroy]
  before_action :set_administrator
  
  def index
    @search = Skill.ransack(params[:search])
    @skills = @search.result
  end

  def show
  end

  def new
    @skill = current_admin_user.skills.build
  end

  def create
    @skill = current_admin_user.skills.build(skill_params)
    if @skill.save
      redirect_to admin_skill_path(@skill), notice: "Successfully Created"
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @skill.update(skill_params)
      redirect_to admin_skill_path(@skill), notice: "Successfully Updated"
    else
      render :edit
    end
  end

  def destroy
    if @skill.destroy
      redirect_to admin_skills_path, notice: "Successfully Deleted"
    end
  end

  private
    def set_skill
      @skill = Skill.find(params[:id])
    end

    def skill_params
      params.require(:skill).permit(:skill_name, :percentage, :user_id)
    end

    def set_administrator
      @admin = User.where({role: "admin"})
    end
end
