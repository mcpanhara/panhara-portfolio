class  FrontEndApplicationController < ActionController::Base
  layout 'front_end_application'
  protect_from_forgery
end
