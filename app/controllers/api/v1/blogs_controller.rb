class Api::V1::BlogsController < FrontEndApplicationController
  def index
    @blogs = Blog.all.order(id: :desc)
    render json: @blogs
  end
end
