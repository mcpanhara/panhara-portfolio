class DemoApplicationController < ActionController::Base
  include Authentication
  layout 'demo_application'
  protect_from_forgery
end
