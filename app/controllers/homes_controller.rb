class HomesController < FrontEndApplicationController
  def index
    @personalities = Personality.all.limit(1)
    @services = Service.all.limit(4)
    @resumes = Experience.all.limit(4)
    @educations = Education.all.limit(4)
    @skills = Skill.all.limit(4)
    @portfolios = Achivement.all.limit(8)
    @blogs = Blog.all.where({status: true}).limit(4).order(id: :desc)
    @settings = Setting.all.limit(1)
    @categoryblogs = Category.all.limit(5)
    @categoryportfloes = Category.all.limit(3)
    @visitors = Visit.all.count
    @cambodia = Visit.where({country: ["Cambodia"]}).count
    @anothercountries = Visit.where.not({country: ["Cambodia"]}).count
    @ahoy_events = Ahoy::Event.all.count
    @contact = Contact.new
  end

  def new
    if request.referrer.split("/").last == "contacts"
      flash[:notice] = nil
    end
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)
      if @contact.save
        respond_to do |format|
          flash.now[:notice] =  "Thank For Submit Your Infomation"
          format.html { redirect_to root_path }
          format.js { render template: '/homes/contact_success.js.erb' }
        end
      else
        respond_to do |format|
          flash.now[:error] = "Message did not send."
          format.html { render 'index' }
          format.js   { render template: '/homes/contact_error.js.erb' }
        end
      end
  end

  private
    def contact_params
      params.require(:contact).permit(:full_name,:email,:phone,:comment)
    end
end
