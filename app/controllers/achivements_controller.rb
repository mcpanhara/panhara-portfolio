class AchivementsController < FrontEndApplicationController
  before_action :set_setting
  def index
    @portfolios = Achivement.all
  end

  def show
    @portfolio = Achivement.find_by_slug(params[:id])
  end

  private
    def set_setting
      @settings = Setting.all.limit(1)
    end
end
