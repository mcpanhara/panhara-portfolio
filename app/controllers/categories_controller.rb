class CategoriesController < FrontEndApplicationController
  def index

  end

  def show

  end

  private
    def set_setting
      @settings = Setting.all.limit(1)
    end
end
