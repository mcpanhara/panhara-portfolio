class Demo::V1::TodosController < DemoApplicationController
  before_action :login_required

  def index
    @incomplete_todos = current_user.todos.where(status: false).order(id: :asc)
    @complete_todos  = current_user.todos.where(status: true).order(id: :asc)
    @todo = current_user.todos.build
    @todos = Todo.all
  end

  def create
    @todo = current_user.todos.build(todo_params)
    if @todo.save
      respond_to do |f|
        flash.now[:notice] = "#{@todo.title} Successful Created"
        f.html { redirect_to demo_v1_todos_url }
        f.js
      end
    else
      respond_to do |f|
        flash.now[:error] = "Not Created"
        f.js { render template: "todos/todo_error.js.erb" }
      end
    end
  end

  def update
    @todo = Todo.find(params[:id])
    @todo.update_attributes!(todo_params)

    respond_to do |f|
      if @todo.status == true
        flash.now[:notice] = "#{@todo.title} is completed"
      else
        flash.now[:error] = "#{@todo.title} is incompleted"
      end
      f.html { redirect_to demo_v1_todos_url }
      f.js
    end
  end

  def destroy
    @todo = Todo.destroy(params[:id])

    respond_to do |f|
      flash.now[:notice] = "#{@todo.title} is removed"
      f.html { redirect_to demo_v1_todos_url }
      f.js
    end
  end

  private
    def todo_params
      params.require(:todo).permit(:title, :status, :user_id)
    end
end
