class Demo::V1::SessionsController < DemoApplicationController

  def new
  end

  def create
    user = DemoUser.authenticate(params[:login], params[:password])
    if user
      session[:demo_user_id] = user.id
      flash[:notice] = "Logged in successfully."
      redirect_to_target_or_default("/demo/v1")
    else
      flash.now[:error] = "Invalid login or password."
      render :action => 'new'
    end
  end

  def destroy
    session[:demo_user_id] = nil
    flash[:notice] = "You have been logged out."
    redirect_to_target_or_default("/demo/v1")
  end
end
