class Demo::V1::DemoUsersController < DemoApplicationController
  def new
    @user = DemoUser.new
  end

  def create
    @user = DemoUser.new(user_params)
    if @user.save
      session[:demo_user_id] = @user.id
      flash.now[:notice] = "Thank you for signing up! You are now logged in."
      redirect_to "/demo/v1"
    else
      render :action => 'new'
    end
  end

  def user_params
    params.require(:demo_user).permit(:username, :email, :password, :salt, :encrypted_password)
  end
end
